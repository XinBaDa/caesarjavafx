package sample;

import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import java.util.Objects;
import java.util.Scanner;

public class CaesarController {
    public TextArea decryptText;
    public TextArea encryptText;
    public TextField keyEnc;
    public Text infoROT;
    public ChoiceBox languageList;
    public Button encButton;
    public Button decButton;


    public void encryptButton(MouseEvent mouseEvent) {
        switch ((String) languageList.getValue()){
            case "RUS":
                encrypt('а','я','А','Я',32);
                break;
            case "ENG":
                encrypt('a','z','A','Z',26);
                break;
        }
    }

    public void decryptButton(MouseEvent mouseEvent) {
        switch ((String) languageList.getValue()){
            case "RUS":
                decrypt('а','я','А','Я',32);
                break;
            case "ENG":
                decrypt('a','z','A','Z',26);
                break;
        }
    }

    public void checkKey(KeyEvent keyEvent) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        if (!keyEnc.getText().matches("[0-9]*")) {
            alert.setTitle("Ошибка!");
            alert.setHeaderText("");
            alert.setContentText("И что это?! Нужно ввести число.");
            alert.showAndWait();
            keyEnc.setText("");
        }
        disableButton();
    }
    public void checkText(KeyEvent keyEvent) {
        disableButton();
    }


    public void encrypt(char alp1, char alp2, char alp1caps,char alp2caps, int modInt) {
            String message, encryptedMessage = "";
            int key;
            char ch;
            Scanner sc = new Scanner(System.in);

            message = encryptText.getText();
            key = Integer.parseInt(keyEnc.getText()) % modInt;

            for (int i = 0; i < message.length(); ++i) {
                ch = message.charAt(i);

                if (ch >= alp1 && ch <= alp2) {
                    ch = (char) (ch + key);

                    if (ch > alp2) {
                        ch = (char) (ch - alp2 + alp1 - 1);
                    }

                    encryptedMessage += ch;
                } else if (ch >= alp1caps && ch <= alp2caps) {
                    ch = (char) (ch + key);

                    if (ch > alp2caps) {
                        ch = (char) (ch - alp2caps + alp1caps - 1);
                    }

                    encryptedMessage += ch;
                } else {
                    encryptedMessage += ch;
                }
            }
            decryptText.setText(encryptedMessage);
        infoROT.setText("(ROT:"+Integer.parseInt(keyEnc.getText())%modInt+")");
    }

    public void decrypt(char alp1, char alp2, char alp1caps,char alp2caps, int modInt) {
            String message, decryptedMessage = "";
            int key;
            char ch;
            Scanner sc = new Scanner(System.in);

            message = encryptText.getText();
            key = Integer.parseInt(keyEnc.getText()) % modInt;

            for (int i = 0; i < message.length(); ++i) {
                ch = message.charAt(i);

                if (ch >= alp1 && ch <= alp2) {
                    ch = (char) (ch - key);

                    if (ch < alp1) {
                        ch = (char) (ch + alp2 - alp1 + 1);
                    }

                    decryptedMessage += ch;
                } else if (ch >= alp1caps && ch <= alp2caps) {
                    ch = (char) (ch - key);

                    if (ch < alp1caps) {
                        ch = (char) (ch + alp2caps - alp1caps + 1);
                    }

                    decryptedMessage += ch;
                } else {
                    decryptedMessage += ch;
                }
            }
            decryptText.setText(decryptedMessage);
        infoROT.setText("(ROT:"+Integer.parseInt(keyEnc.getText())%modInt+")");
    }

    public void disableButton(){
        if((!Objects.equals(keyEnc.getText(), ""))&&(!Objects.equals(encryptText.getText(), ""))){
            encButton.setDisable(false);
            decButton.setDisable(false);
        }
        else{
            encButton.setDisable(true);
            decButton.setDisable(true);
        }
    }

}